#!/usr/bin/env bash
BUCKET=overwatchvision-userfiles-mobilehub-1020206096
DIR=./s3-uploads

echo "Syncing images from S3..."
aws s3 sync s3://$BUCKET/uploads $DIR
