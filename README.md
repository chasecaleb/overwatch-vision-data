# Overwatch Vision: Training Data

> Accompoanying training data for https://github.com/chasecaleb/OverwatchVision

## Overview

- Prerequisite: install AWS CLI and run `aws configure` to setup credentials
- `sync-s3.sh`: Sync (download) images from S3 bucket
- `process-images.sh`: Record expected recognition data
