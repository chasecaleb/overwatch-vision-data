#!/usr/bin/env bash
DIR=./s3-uploads
declare -A OUTCOME_OPTS=(
    [vic]="victory"
    [def]="defeat"
    [dra]="draw"
)
declare -A MAP_OPTS=(
    [bli]="blizzard_world"
    [dor]="dorado"
    [eic]="eichenwalde"
    [han]="hanamura"
    [hly]="hollywood"
    [hor]="horizon"
    [ili]="ilios"
    [jun]="junkertown"
    [kin]="kings_row"
    [lij]="lijiang_tower"
    [nep]="nepal"
    [num]="numbani"
    [oas]="oasis"
    [ria]="rialto"
    [rou]="route_66"
    [tem]="temple_of_anubis"
    [vol]="volskaya"
    [wat]="watchpoint_gibraltar"
)
declare -A HERO_OPTS=(
    [all]="all_heroes"
    [ana]="ana"
    [bas]="bastion"
    [bri]="brigitte"
    [doo]="doomfist"
    [dva]="dva"
    [gen]="genji"
    [han]="hanzo"
    [jun]="junkrat"
    [luc]="lucio"
    [mcc]="mccree"
    [mei]="mei"
    [mer]="mercy"
    [moi]="moira"
    [ori]="orisa"
    [pha]="pharah"
    [rea]="reaper"
    [rei]="reinhardt"
    [roa]="roadhog"
    [sol]="soldier_76"
    [som]="sombra"
    [sym]="symmetra"
    [tor]="torbjorn"
    [tra]="tracer"
    [wid]="widowmaker"
    [win]="winston"
    [wre]="wrecking_ball"
    [zar]="zarya"
    [zen]="zenyatta"
)

process-img() {
    local IMG=$1
    local OUT=$IMG.data
    if [[ -f $OUT ]]; then
        return
    fi

    echo "PROCESSING: $IMG"
    feh --scale-down $IMG &
    local FEH_PID=$!

    local ROTATED
    while [[ ! $ROTATED =~ ^[yn]$ ]]; do
        read -p "Rotate image [y/n]: " -r ROTATED
    done
    if [[ $ROTATED =~ ^[yY]$ ]]; then
        convert -rotate 180 $IMG $IMG

        # Re-open to display correctly rotated version.
        kill $FEH_PID
        feh --scale-down $IMG &
        FEH_PID=$!
    fi

    # Associative arrays are in hash order, so...
    OUTCOME_MENU=$(for OPT in "${!OUTCOME_OPTS[@]}"; do echo -e "\t[$OPT]: ${OUTCOME_OPTS[$OPT]}"; done)
    echo -e "$OUTCOME_MENU" | sort -t ":" -k 2
    local OUTCOME
    while [[ -z $OUTCOME ]]; do
        read -p "Outcome: "
        OUTCOME=${OUTCOME_OPTS[$REPLY]}
    done

    echo "Map options:"
    MAP_MENU=$(for OPT in "${!MAP_OPTS[@]}"; do echo -e "\t[$OPT]: ${MAP_OPTS[$OPT]}"; done)
    echo -e "$MAP_MENU" | sort -t ":" -k 2
    local MAP
    while [[ -z $MAP ]]; do
        read -p "Map: "
        MAP=${MAP_OPTS[$REPLY]}
    done

    echo "Hero options:"
    HERO_MENU=$(for OPT in "${!HERO_OPTS[@]}"; do echo -e "\t[$OPT]: ${HERO_OPTS[$OPT]}"; done)
    echo -e "$HERO_MENU" | sort -t ":" -k 2
    local HERO
    while [[ -z $HERO ]]; do
        read -p "Hero: "
        HERO=${HERO_OPTS[$REPLY]}
    done

    local STATS
    while [[ ! $STATS =~ ^([0-9]{1,5} ){5}[0-9]{1,5}$ ]]; do
        read -p "Stats (left to right, space-separated): " -r STATS
    done
    # Remove any user-entered formatting and convert to comma-separated
    STATS=$(echo $STATS | sed 's/[,:]//g' | sed 's/ /,/g')

    kill $FEH_PID

    echo "Recorded data:"
    tee $OUT <<EOF
outcome=$OUTCOME
map=$MAP
hero=$HERO
stats=$STATS
EOF

    echo ""
}

for F in $DIR/*.jpg; do
    process-img $F
done
